from app.data.Context import DataOutput, DataOptions, DataContext
from app.data.DataProcessor import DataProcessor
from app.reader.DataReader import DataReader
from app.writer.DataWriter import DataWriter

if __name__ == '__main__':
    # setting context
    filename = "app/input.txt"
    output = DataOutput.JSON
    duplicates = DataOptions.NO_DUPLICATES
    context = DataContext(output, DataOptions.DUPLICATES)

    # reading data
    reader = DataReader(filename, context)
    reader.read()
    data = reader.get_data()

    # process
    processor = DataProcessor(data, context)
    processor.process()
    data = processor.get_data()

    # write it out
    writer = DataWriter(data, context)
    writer.write()

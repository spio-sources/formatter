import json


class DataWriter(object):
    SEP = ";"

    def __init__(self, data, context):
        self.data = data
        self.context = context

    def write(self):
        if self.context.is_csv():
            return self.write_csv()
        elif self.context.is_json():
            return self.write_json()
        else:
            print("Test")

    def write_csv(self):
        for tup in self.data:
            line = ""
            for item in tup:
                line = line + item + self.SEP
            print(line)

    def write_json(self):
        header = ["Name", "Surname", "Dept", "Func"]
        turbo_list = []
        for tup in self.data:
            zipped = zip(header, tup)
            turbo_list.append(dict(zipped))
        print(json.dumps(turbo_list, indent=4))

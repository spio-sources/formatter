class DataReader(object):

    def __init__(self, filename, context):
        self.filename = filename
        self.context = context
        self.data = []

    def read(self):
        f = open(self.filename)
        for line in f:
            words = line.strip().split('\t')
            line_tuple = tuple(words)
            self.data.append(line_tuple)
        pass;

    def get_data(self):
        return self.data

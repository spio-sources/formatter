from enum import Enum


class DataOutput(Enum):
    CSV = 1
    JSON = 2


class DataOptions(Enum):
    NO_DUPLICATES = 1
    DUPLICATES = 2


class DataContext(object):
    def __init__(self, output, option):
        self.output = output
        self.option = option

    def is_csv(self):
        return self.output == DataOutput.CSV

    def is_json(self):
        return self.output == DataOutput.JSON

    def is_duplicates_not_allowed(self):
        return self.option == DataOptions.NO_DUPLICATES

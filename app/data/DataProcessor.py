class DataProcessor(object):
    def __init__(self, data, context):
        self.data = data
        self.context = context

    def process(self):
        if self.context.is_duplicates_not_allowed():
            self.data = list(set(self.data))

    def get_data(self):
        return self.data
